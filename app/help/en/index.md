# Graphene Help

Dxperts 2.0 is a Financial Smart Contracts platform that is based on the
[Graphene](https://github.com/cryptonomex/graphene) technology built by
[Cryptonomex](http://cryptonomex.com). You can see Graphene as a toolkit or
technology for real-time blockchains.

This help section gives a brief overview and describes the most basic concepts
of this application.

## Introduction 
 * [Dxperts](introduction/bitshares.md)
 * [Wallet](introduction/wallets.md)
 * [Backups](introduction/backups.md)
 * [Blockchain](introduction/blockchain.md)
 * [BlockProducer](introduction/witness.md)
 * [BeneFactor](introduction/workers.md)
 * [Committe Members](introduction/committee.md)

## Accounts
 * [Introduction](accounts/general.md)
 * [Permissions](accounts/permissions.md)
 * [Memberships](accounts/membership.md)

## Assets
 * [Market Pegged Assets](assets/mpa.md) (ᴅUSD, ᴅEUR,ᴅ\*,...)
 * [User Issued Assets](assets/uia.md)
 * [Privatized SmartAssets](assets/privbitassets.md)

## Decentralized Exchange
 * [Introduction](dex/introduction.md)
 * [Trading](dex/trading.md)
 * [Short Selling SmartAssets](dex/shorting.md)

## Development
 * [Dxperts UI Github](https://github.com/bitshares/bitshares-ui)

----------
[Glossary](glossary.md)
